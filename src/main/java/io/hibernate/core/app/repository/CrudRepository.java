package io.hibernate.core.app.repository;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T, ID> {

    Optional<T> findById(ID value);

    List<T> findAll();

    T save(T value);

    T update(T value);

    Boolean removeById(ID value);

    Boolean remove(T value);
}
