package io.hibernate.core.app.dao;

import io.hibernate.core.app.entity.Jurusan;
import io.hibernate.core.app.repository.CrudRepository;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

public class JurusanDao implements CrudRepository<Jurusan, Long> {

    private Session session;

    public JurusanDao(Session session) {
        this.session = session;
    }

    @Override
    public Optional<Jurusan> findById(Long value) {
        Jurusan result = (Jurusan) this.session.createSQLQuery("SELECT * FROM jurusan j WHERE id=:id")
                .addEntity("j", Jurusan.class)
                .setParameter("id", value).getSingleResult();
        return result != null ? Optional.of(result) : Optional.empty();
    }

    @Override
    public List<Jurusan> findAll() {
        List<Jurusan> jurusans = null;
        try {
            jurusans = this.session.createSQLQuery("SELECT * FROM jurusan j").addEntity("j", Jurusan.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jurusans;
    }

    @Override
    public Jurusan save(Jurusan value) {
        Long id = (Long) this.session.save(value);
        value.setId(id);
        return value;
    }

    @Override
    public Jurusan update(Jurusan value) {
        this.session.update(value);
        return value;
    }

    @Override
    public Boolean removeById(Long value) {
        return null;
    }

    @Override
    public Boolean remove(Jurusan value) {
        try {
            this.session.remove(value);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
