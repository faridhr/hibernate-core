package io.hibernate.core.app.dao;

import io.hibernate.core.app.entity.Jurusan;
import io.hibernate.core.app.entity.Mahasiswa;
import io.hibernate.core.app.repository.CrudRepository;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

public class MahasiswaDao implements CrudRepository<Mahasiswa, Long> {

    private Session session;

    public MahasiswaDao(Session session) {
        this.session = session;
    }

    @Override
    public Optional<Mahasiswa> findById(Long value) {
        Mahasiswa mahasiswa = this.session.find(Mahasiswa.class, value);
        return mahasiswa != null ? Optional.of(mahasiswa) : Optional.empty();
    }

    @Override
    public List<Mahasiswa> findAll() {
        List<Mahasiswa> mahasiswas = null;
        try {
            mahasiswas = this.session.createSQLQuery("select * from mahasiswa m  left join jurusan j on m.jurusan_id = j.id")
                    .addEntity("m", Mahasiswa.class)
                    .list();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return mahasiswas;
    }

    @Override
    public Mahasiswa save(Mahasiswa value) {
        Long dataID = (Long) this.session.save(value);
        value.setId(dataID);
        return value;
    }

    @Override
    public Mahasiswa update(Mahasiswa value) {
        this.session.update(value);
        return value;
    }

    @Override
    public Boolean removeById(Long value) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public Boolean remove(Mahasiswa value) {
        try {
            this.session.remove(value);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
