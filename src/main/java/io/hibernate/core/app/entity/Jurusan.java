package io.hibernate.core.app.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "jurusan")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Jurusan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer quota;
}
