package io.hibernate.core.app.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "mahasiswa")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Mahasiswa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String nim;

    @Column(nullable = false)
    private String nama;

    private LocalDate tahunMasuk;

    @Column(nullable = false)
    private LocalDate tanggalLahir;

    @Column(nullable = false)
    private LocalDateTime createdDate;

    private String createdBy;

    @Column(name = "is_active")
    private Boolean active;

    private String biodata;

    @ManyToOne
    private Jurusan jurusan;
}
