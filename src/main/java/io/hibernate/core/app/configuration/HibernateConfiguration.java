package io.hibernate.core.app.configuration;

import io.hibernate.core.app.entity.Jurusan;
import io.hibernate.core.app.entity.Mahasiswa;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

@Slf4j
public class HibernateConfiguration {

    private static final SessionFactory sessionFactory;

    static {
        final StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .configure().build();

        try {
            MetadataSources metadataSources = new MetadataSources(serviceRegistry);
            metadataSources.addAnnotatedClass(Mahasiswa.class);
            metadataSources.addAnnotatedClass(Jurusan.class);
            sessionFactory = metadataSources.buildMetadata().buildSessionFactory();

        }catch (Throwable e) {
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }
}
