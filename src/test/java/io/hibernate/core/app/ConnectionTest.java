package io.hibernate.core.app;

import io.hibernate.core.app.configuration.HibernateConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@Slf4j
public class ConnectionTest {

    private Session session;

    @BeforeEach
    void setUp() {
        log.info("Getting session");
        this.session = HibernateConfiguration.getSession();
    }

    @AfterEach
    void tearDown() {
        log.info("Closing Session");
        this.session.close();
    }

    @Test
    void connectionTest() {
        this.session.beginTransaction();
    }
}
