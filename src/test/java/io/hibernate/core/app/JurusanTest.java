package io.hibernate.core.app;

import io.hibernate.core.app.configuration.HibernateConfiguration;
import io.hibernate.core.app.dao.JurusanDao;
import io.hibernate.core.app.entity.Jurusan;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@Slf4j
public class JurusanTest {

    private Session session;

    private JurusanDao jurusanDao;

    @BeforeEach
    void setUp() {
        this.session = HibernateConfiguration.getSession();
        this.jurusanDao = new JurusanDao(session);
    }

    @AfterEach
    void tearDown() {
        this.session.close();
    }

    @Test
    void testCreateJurusan() {
        Jurusan jurusan = Jurusan.builder()
                .name("Bimbingan Konseling")
                .quota(400)
                .build();

        this.session.beginTransaction();
        Jurusan result = this.jurusanDao.save(jurusan);
        this.session.getTransaction().commit();
        log.info(result.toString());
    }

    @Test
    void testUpdateJurusan() {
        Jurusan jurusan = Jurusan.builder()
                .id(2L)
                .name("Bimbingan Konseling")
                .quota(200)
                .build();

        this.session.beginTransaction();
        Jurusan result = this.jurusanDao.update(jurusan);
        this.session.getTransaction().commit();
        log.info(result.toString());
    }

    @Test
    void testFindByIdJurusan() {
        Jurusan jurusan = this.jurusanDao.findById(2L).get();
        log.info(jurusan.toString());
    }
}
