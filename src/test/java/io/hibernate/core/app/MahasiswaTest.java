package io.hibernate.core.app;

import io.hibernate.core.app.configuration.HibernateConfiguration;
import io.hibernate.core.app.dao.MahasiswaDao;
import io.hibernate.core.app.entity.Mahasiswa;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
public class MahasiswaTest {

    private Session session;
    private MahasiswaDao mahasiswaDao;

    @BeforeEach
    void setUp() {
        this.session = HibernateConfiguration.getSession();
        this.mahasiswaDao = new MahasiswaDao(this.session);
    }

    @AfterEach
    void tearDown() {
        this.session.close();
    }

    @Test
    void testFindAllMahasiswa() {
        this.session.beginTransaction();

        List<Mahasiswa> mahasiswas = this.mahasiswaDao.findAll();

        this.session.getTransaction().commit();
        Assertions.assertEquals(2, mahasiswas.size());
        log.info("List of Mahasiswa: {}", mahasiswas);
    }

    @Test
    void testCreateMahasiswa() {
        Mahasiswa mahasiswa = Mahasiswa.builder()
                .nim("201551048")
                .nama("Farid HR")
                .active(true)
                .tanggalLahir(LocalDate.of(1997, 01, 14))
                .tahunMasuk(LocalDate.of(2015, 07, 10))
                .createdDate(LocalDateTime.now())
                .createdBy("Farid")
                .biodata("Farid HR")
                .build();

        this.session.beginTransaction();

        Mahasiswa mahasiswaSave = this.mahasiswaDao.save(mahasiswa);

        this.session.getTransaction().commit();

        log.info("Create new mahasiswa successfully");

    }

    @Test
    void testUpdateMahasiswa() {
        Mahasiswa mahasiswa = Mahasiswa.builder()
                .id(1L)
                .nim("201551048")
                .nama("Farid Hidayatur Rahman")
                .active(true)
                .tanggalLahir(LocalDate.of(1997, 01, 20))
                .tahunMasuk(LocalDate.of(2015, 07, 10))
                .createdDate(LocalDateTime.now())
                .createdBy("Farid")
                .biodata("Farid HR (Updated)")
                .build();

        this.session.beginTransaction();

        this.mahasiswaDao.update(mahasiswa);

        this.session.getTransaction().commit();

        log.info("Update Mahasiwa with id={} successfully", mahasiswa.getId());
    }

    @Test
    void testMahasiswaFindById() {
        this.session.beginTransaction();

        Optional<Mahasiswa> mahasiswa = this.mahasiswaDao.findById(1L);
        Optional<Mahasiswa> mahasiswa2 = this.mahasiswaDao.findById(2L);

        this.session.getTransaction().commit();

        Assertions.assertTrue(mahasiswa.isPresent(), "Mahasiswa is not null");
        Assertions.assertFalse(mahasiswa2.isPresent(), "Mahasiswa with id= " + 2L + " is null");

        log.info("Data Mahasiswa : {}", mahasiswa.get());

    }

    @Test
    void testDeleteMahasiswa() {
        this.session.beginTransaction();

        Mahasiswa mahasiswa = this.mahasiswaDao.findById(3L).get();
        Boolean deleteMahasiswa = this.mahasiswaDao.remove(mahasiswa);

        this.session.getTransaction().commit();

        Assertions.assertTrue(deleteMahasiswa);
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> this.mahasiswaDao.findById(5L).get()
        );
    }
}
